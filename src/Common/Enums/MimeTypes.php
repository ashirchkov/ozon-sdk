<?php


namespace AlexeyShirchkov\Ozon\Common\Enums;


class MimeTypes
{

    public const JSON_CONTENT_TYPE = 'application/json';

    public const STREAM_CONTENT_TYPE = 'application/octet-stream';

    public const MULTIPART_CONTENT_TYPE = 'multipart/form-data';

    public const FORM_URLENCODED_TYPE = 'application/x-www-form-urlencoded';

}