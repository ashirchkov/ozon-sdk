<?php

namespace AlexeyShirchkov\Ozon\Common\Enums;

class HttpMethods
{

    const POST = 'POST';

    const GET = 'GET';

    const PUT = 'PUT';

    const DELETE = 'DELETE';

    const HEAD = 'HEAD';

    const CONNECT = 'CONNECT';

    const OPTIONS = 'OPTIONS';

    const TRACE = 'TRACE';

    const PATH = 'PATH';


}